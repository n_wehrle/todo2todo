![yza9gu7ipdlueng3bgl7.jpg](https://bitbucket.org/repo/Azj8ad/images/636999698-yza9gu7ipdlueng3bgl7.jpg)

## What you get
* a tool that converts all your data **from Cultured Code Things to Wunderlist**
* no manual post-processing in most cases
* three simple steps: start the converter, import into Wunderlist, be happy
* privacy: the conversion is performed on your machine (no cloud)
* **needs OSX Yosemite**

![20150424072853-DSCF3466.jpg](https://bitbucket.org/repo/Azj8ad/images/1348705988-20150424072853-DSCF3466.jpg)

## Motivation
I love Cultured Code Things. It is simple and I can apply all the concepts of GTD - or only a few. Every device I own (Mac, iPhone and iPad) has their own version of Things. The usability on every supported platform is brilliant. The Synchronization is fast and stable...

BUT:

I am vendor locked to Apple Devices. I like my Mac very much, but sometimes I have to use a windows laptop.
Or what if my next device is an Android device? Conclusion: A multiplatform solution is needed, covering OSX, Windows, Android, Web, iOS ..

Cultured Code Things is the only app that prevents me from switching platforms - in my case from iOS to Android. I spent a great deal of time populating my GTD system, which I'd rather not do again. I need a tool that converts all my todos from Things to a shiny new multiplatform GTD tool. My research has identified Wunderlist and Todoist to be both perfectly suited. My plan is to support both, but I will start with Wunderlist.

![20150407134644-2do22do_1.jpg](https://bitbucket.org/repo/Azj8ad/images/197674212-20150407134644-2do22do_1.jpg)

## Transformation rules
Things and Wunderlist support GTD differently. In Things every subtask can have the same metadata as a task. In wunderlist subtasks have neither tags, notes or due dates.

My aim is to transfer all available information from Things to Wunderlist. That is why I will introduce the following mapping, which might be optimised adapted as the project moves along.

* Things projects will become Wunderlist tasks
* Things top-level tasks will become Wunderlist tasks
* notes, tags and dates that belong to a Things project or top-level task will become notes, tags and dates on a Wunderlist task
* tasks within Things projects will become Wunderlist sub-tasks
* the horizon of focus in Things will become a tag in Wunderlist
* areas of responsibility end up as Lists in Wunderlist
* tasks that are planned for "today" in Things will be starred in Wunderlist
* dates and notes of Things projects tasks will be attached to the  parent task's note in Wunderlist
* tags that are on a Things project task will be attached to the parent task in Wunderlist
* the Things inbox will become an ordinary list in Wunderlist (please move the few items in there by hand to the Wunderlist inbox)
* Things tasks that don't have an area of responsibility will be placed in a default list in Wunderlist
* If you use persons in Things, your tasks can be found in a list named after the area of responsibility. For now tagging the task with the persons name is not planned, as I believe the feature is rarely used
* Reoccuring tasks will not be exported from things (in the initial release)

## Recommended usage
* download the [compressed binary](https://bitbucket.org/n_wehrle/todo2todo/downloads/things2wunderlist.app.zip)
* unzip
* run (needs OSX Yosemite)

You will receive a file wunderlist-import.json on your desktop. Import that file into wunderlist using it's backup restore feature, see the video clip below.

## Commandline usage
* checkout or download the repository
* open shell (needs OSX Yosemite)

```
#!shellscript

osascript -l JavaScript things2wunderlist.js
```


You will receive a file wunderlist-import.json on your desktop. Import that file into wunderlist using it's backup restore feature, see the video clip below.

## Experimental usage
* open Yosemite ScriptEditor
* create new File (type JavaScript, needs OSX Yosemite)
* paste the content of things2wunderlist.js
* play around, edit, delete, configure
* press the play button

You will receive a file wunderlist-import.json on your desktop. Import that file into wunderlist using it's backup restore feature, see the video clip below.

## Import into Wunderlist
* Open Wunderlist.com in your browser
* log in to your account
* goto account settings
* import backup data
* choose the file wunderlist-import.json from your desktop

## Video walkthrough
Export, import, filtering in less than 120s
https://youtu.be/XuSXikTkdJc

## Configuration
The script comes with useful defaults and runs without configuration. If you want to customize the behaviour, you can edit the config object in the first fifty lines.


```
#!javascript

var config = {
    /* the user-id in wunderlist, a random value works, the real id is better */
    user: getRandomInt(9000000000, 10000000000),

    /* import from these things-focus-lists */
    lists: ['Inbox', 'Today', 'Next', 'Scheduled', 'Someday'],
    progressDescription: 'Exporting tasks from Cultured Code Things',

    exportFile: '~/Desktop//wunderlist-import.json',

    /* this focuslist will be suppressed because all the task are in 'Next', to */
    todayFocusListName: 'Today',

    /* all items without an area will be saved here */
    defaultArea: {name: 'No Area', id: 'defaultAreaId'},

    /* all items from focuslist 'Inbox' will be saved in this wunderlist */
    inboxArea: {name: 'Inbox', id: 'inboxAreaId'},

    /* focuslists and projects will get special tags in wunderlist */
    tagNames: {
        Next: '#next',
        Scheduled: '#scheduled',
        Someday: '#someday',
        Project: '#project'
    },

    /* remind me at 8 o'clock */
    reminderOffset: 8,

    /*
    		subtasks in things can have metadata.
    		subtasks in wunderlist cannot.
    		if true, metadata from things will be merged into the parent task
    	*/
    consolidateTags: true,
    consolidateNotes: true,
    consolidateDates: true,

    /* Dates will become notes when merged, here are the labels */
    consolidateDueDate: 'due',
    consolidateActivationDate: 'activate'
};
```

## Copyright
The forklift logo has been designed by [Allessandra Antonetti](http://thenounproject.com/alessandrantonetti/)