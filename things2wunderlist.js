var config = {
    /* the user-id in wunderlist, a random value works, the real id is better */
    user: getRandomInt(9000000000, 10000000000),

    /* import from these things-focus-lists */
    lists: ['Inbox', 'Today', 'Next', 'Scheduled', 'Someday'],
    progressDescription: 'Exporting tasks from Cultured Code Things',

    exportFile: '~/Desktop//wunderlist-import.json',

    /* this focuslist will be suppressed because all the task are in 'Next', to */
    todayFocusListName: 'Today',

    /* all items without an area will be saved here */
    defaultArea: {name: 'No Area', id: 'defaultAreaId'},

    /* all items from focuslist 'Inbox' will be saved in this wunderlist */
    inboxArea: {name: 'Inbox', id: 'inboxAreaId'},

    /* focuslists and projects will get special tags in wunderlist */
    tagNames: {
        Next: '#next',
        Scheduled: '#scheduled',
        Someday: '#someday',
        Project: '#project'
    },

    /* remind me at 8 o'clock */
    reminderOffset: 8,

    /*
    		subtasks in things can have metadata.
    		subtasks in wunderlist cannot.
    		if true, metadata from things will be merged into the parent task
    	*/
    consolidateTags: true,
    consolidateNotes: true,
    consolidateDates: true,

    /* Dates will become notes when merged, here are the labels */
    consolidateDueDate: 'due',
    consolidateActivationDate: 'activate'
};

var things = things2json(config);
var wunderlist = json2wunderlist(things, config);
writeObjectToFile(wunderlist, config.exportFile);

/* ========================================================================== */

function writeObjectToFile(object, fileName) {
    var expandedExportfile = $.NSString.alloc.initWithUTF8String(fileName).stringByExpandingTildeInPath;
    var bridgedString = $.NSString.alloc.initWithUTF8String(JSON.stringify(object));
    bridgedString.writeToFileAtomicallyEncodingError(expandedExportfile, true, $.NSUTF8StringEncoding, null);
}

// Returns a random integer between min (included) and max (excluded)
// Using Math.round() will give you a non-uniform distribution!
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

/* ========================================================================== */

function things2json(config) {
    'use strict';

    var result = {
        config: {},
        data: {},
        contacts: {}
    };

    var  app = Application('Things'),
    isTrashed = todoFilter(app, 'Trash');

    // only an approximation, there may be less or more steps
    // the progressbar handles higher values graceful
    var todoCount = 0;
    config.lists.forEach(function(listName) {
        todoCount += app.lists[listName].toDos.length;
    });

    Progress.totalUnitCount = todoCount;
    Progress.additionalDescription = config.progressDescription;

    // fill the configure block with life
    configureResult(result);

    // create the time based lists
    config.lists.forEach(function(listName) {
        handleList(listName, result.data, app.lists[listName].toDos);
    });

    // create the contact based lists
    var contacts = app.contacts;
    if (contacts) {
        for (var h = 0; h < contacts.length; h++) {
            var currentContact = contacts[h];
            handleList(currentContact.name(), result.contacts, currentContact.toDos);
        }
    }

    return result;

    /* ====================================================================== */

    function handleList(listName, parentNode, outerTodos) {
        parentNode[listName] = {
            todos: {},
            projects: {}
        };

        var outerTodosLength = outerTodos.length;
        for (var i = 0; i < outerTodosLength; i++) {
            Progress.completedUnitCount++;

            var currentOuterTodo = outerTodos[i];
            if (!currentOuterTodo.project() && !currentOuterTodo.toDos.length) {
                /* simple todo, not in a project */
                if (currentOuterTodo.status() === 'open' && !isTrashed(currentOuterTodo)) {
                    parentNode[listName].todos[currentOuterTodo.id()] = (objectForTodo(currentOuterTodo));
                }
            } else if (currentOuterTodo.project()) {
                /* simple todo within a project */
                appendProjectToNode(currentOuterTodo.project(), parentNode[listName].projects);
            } else if (currentOuterTodo.toDos.length) {
                /* i am a project */
                appendProjectToNode(currentOuterTodo, parentNode[listName].projects);
            }
        }
    }

    function configureResult(result) {
        result.config.areas = [];

        var areas = app.areas;
        var areasLength = app.areas.length;
        for (var i = 0; i < areasLength; i++) {
            var currentArea = areas[i];
            result.config.areas.push(
				{
    id: currentArea.id(),
    name: currentArea.name()
				}
			);
        }
    }

    function appendProjectToNode(project, node) {
        var id = project.id();
        if (!node[id]) {
            node[id] = objectForTodo(project);
            node[id].todos = {};
            appendOpenTodosToNode(project.toDos, node[id].todos);
        }
    }

    function appendOpenTodosToNode(todos, node) {
        var length = todos.length;
        for (var j = 0; j < length; j++) {
            var currentTodo = todos[j];
            if (currentTodo.status() === 'open' && !isTrashed(currentTodo)) {
                node[currentTodo.id()] = objectForTodo(currentTodo);
            }
        }
    }

    function objectForTodo(todo) {
        var object = {
            id                : todo.id(),
            name              : todo.name(),
            status            : todo.status(),
            notes             : todo.notes(),
            creationDate      : todo.creationDate(),
            modificationDate  : todo.modificationDate(),
        };

        var tagNames = todo.tagNames();
        if (tagNames) {
            object.tagNames = tagNames.split(/,\s*/);
        }

        var area = todo.area();
        if (area) {
            object.area = {
                id: area.id(),
                name: area.name()
            };
        }

        var dueDate = todo.dueDate();
        if (dueDate) {
            object.dueDate = dueDate;
        }

        var activationDate = todo.activationDate();
        if (activationDate) {
            object.activationDate = activationDate;
        }

        return object;
    }

    function todoFilter(app, listName) {
        var map = {},
            todos = app.lists[listName].toDos,
            length = todos.length;

        for (var i = 0; i < length; i++) {
            var id = todos[i].id();
            map[id] = true;
        }

        return function(todo) {
            return !!map[todo.id()];
        };
    }
}

/* ========================================================================== */

function json2wunderlist(things, config) {
    var wunderlist = {
        user: config.user,
        exported: (new Date()).toISOString(),
        data: {
            lists: [],
            tasks: [],
            reminders: [],
            subtasks: [],
            notes: [],
            task_positions: [],
            subtask_positions: []
        }
    };

    // generate unique ids for each object type
    var listIdFactory = idFactory();
    var taskIdFactory = idFactory();
    var noteIdFactory = idFactory();
    var reminderIdFactory = idFactory();
    var subtaskIdFactory = idFactory();

    // do json transfromations
    createWunderLists(things, wunderlist);
    return wunderlist;

    /* ====================================================================== */

    function createWunderLists(things, wunderlist) {
        for (var focusListName in things.data) {
            //exclude the focuslist for today, the task are in the next-list, too
            if (config.todayFocusListName != focusListName) {
                createEntriesForTodos(focusListName, things.data[focusListName].todos);
                createEntriesForTodos(focusListName, things.data[focusListName].projects);
            }
        }
    }

    function createEntriesForTodos(focusListName, todos) {
        'use strict';
        var todoId, currentTodo, currentArea,
            wList, wlId,
            wlTaskPosition, wlTask, wlTaskId,
            wlNote, wlNoteId, wlReminderId, wlReminder,
            putStarToTasks;

        for (todoId in todos) {
            currentTodo = todos[todoId];

            if (focusListName === 'Inbox') {
                currentArea = config.inboxArea;
            } else {
                currentArea = currentTodo.area || config.defaultArea;
            }

            wList = findObject(wunderlist.data.lists, 'things_area_id', currentArea.id);

            if (!wList) {
                wlId = listIdFactory();

                wList = listForThingsArea(currentArea, wlId);
                wunderlist.data.lists.push(wList);

                wlTaskPosition = taskPositionForId(wlId);
                wunderlist.data.task_positions.push(wlTaskPosition);
            } else {
                wlId = wList.id;
                wlTaskPosition = findObject(wunderlist.data.task_positions, 'id', wlId);
            }

            wlTaskId = taskIdFactory();

            //this todo is also in the today focuslist
            putStarToTasks = things.data[config.todayFocusListName] &&
            	things.data[config.todayFocusListName].todos &&
            	!!things.data[config.todayFocusListName].todos[currentTodo.id];

            wlTask = taskForThingsTodo(currentTodo, {
                wlTaskId: wlTaskId,
                wlId: wlId,
                creator: config.user,
                star: putStarToTasks,
                focusTag: config.tagNames[focusListName]
            });
            wunderlist.data.tasks.push(wlTask);
            wlTaskPosition.values.push(wlTaskId);

            if (currentTodo.notes) {
                wlNoteId = noteIdFactory();
                wlNote = noteForThingsTodo(currentTodo, wlNoteId, wlTaskId);
                wunderlist.data.notes.push(wlNote);
            }

            if (currentTodo.activationDate) {
                wlReminderId = reminderIdFactory();
                wlReminder = reminderForThingsTodo(currentTodo, wlReminderId, wlTaskId, config.reminderOffset);
                wunderlist.data.reminders.push(wlReminder);
            }

            appendThingsTodosAsSubtasks(currentTodo, wlTask);
        }
    }

    function findObject(array, key, value) {
        if (null === array) return false;
        var arrayLength = array.length;
        for (var i = 0; i < arrayLength; i++) {
            var object = array[i];
            if (key) {
                if (object[key] === value) {
                    return object;
                }
            } else {
                if (object === value) {
                    return object;
                }
            }
        }
        return undefined;
    }

    function idFactory() {
        var id = 1;

        return function() {
            return id++;
        };
    }

    function listForThingsArea(area, wlId) {
        return {
            id             : wlId,
            created_at     : (new Date()).toISOString(),
            list_type      : 'list',
            'public'       : false,
            revision       : 1,
            title          : area.name,
            type           : 'list',
            things_area_id : area.id
        };
    }

    function taskPositionForId(wlId) {
        return {
            id       : wlId,
            list_id  : wlId,
            revision : 1,
            values   : [],
            type     : 'task_position'
        };
    }

    function subtaskPositionForId(wlTaskId) {
        return {
            id       : wlTaskId,
            task_id  : wlTaskId,
            revision : 1,
            values   : [],
            type     : 'subtask_position'
        };
    }

    function taskForThingsTodo(todo, params) {
        var wlTask = {
            id             : params.wlTaskId,
            created_at     : todo.creationDate,
            created_by_id  : params.creator,
            completed      : false,
            starred        : params.star,
            list_id        : params.wlId,
            revision       : 1,
            title          : todo.name,
            type           : 'task',
            things_todo_id : todo.id,
            things_name    : todo.name,
            things_tagNames: todo.tagNames
        };

        if (params.focusTag) {
            wlTask.title += ' ' + params.focusTag;
        }

        if (todo.todos) {
            wlTask.title += ' ' + config.tagNames.Project;
        }

        if (todo.dueDate) {
            var dueDate = new Date(todo.dueDate);
            wlTask.due_date = dueDate.getFullYear() + '-' + (dueDate.getMonth() + 1) + '-' + dueDate.getDate();
        }

        if (todo.tagNames) {
            todo.tagNames.forEach(function(tagName) {
                wlTask.title += ' #' + tagName.replace(/\s/g, '-');
            });
        }

        return wlTask;
    }

    function subtaskForThingsTodo(todo, params) {
        var wlSubtask = {
            id             : params.wlSubtaskId,
            created_at     : todo.creationDate,
            created_by_id  : params.creator,
            completed      : false,
            task_id        : params.wlTaskId,
            revision       : 1,
            title          : todo.name,
            type           : 'subtask',
            things_todo_id : todo.id,
            things_name    : todo.name,
            things_tagNames: todo.tagNames
        };

        return wlSubtask;
    }

    function noteForThingsTodo(todo, wlNoteId, wlTaskId) {
        return {
            id          : wlNoteId,
            revision    : 1,
            content     : todo.notes,
            type        : 'note',
            task_id     : wlTaskId
        };
    }

    function reminderForThingsTodo(todo, wlReminderId, wlTaskId, reminderOffset) {
        var reminderDate = new Date(todo.activationDate);
        reminderDate.setHours(reminderOffset);

        return {
            id         : wlReminderId,
            date       : reminderDate.toISOString(),
            task_id    : wlTaskId,
            revision   : 1,
            created_at : todo.creationDate,
            updated_at : todo.modificationDate,
            type       : "reminder"
        };
    }

    function mergeTodoIntoProject(todo, project, wlTask) {
        // merge tags to parent
        if (config.consolidateTags && todo.tagNames) {
            todo.tagNames.forEach(function(tagName) {
                if (!project.tagNames || !findObject(project.tagNames, null, tagName)) {
                    wlTask.title += ' #' + tagName.replace(/\s/g, '-');
                }
            });
        }

        // merge into parent note
        var mustMergeNotes = config.consolidateNotes && todo.notes;
        var mustMergeDates = config.consolidateDates && (todo.dueDate || todo.activationDate);
        var mustMergeAnything = mustMergeNotes || mustMergeDates;

        if (mustMergeAnything) {
            // create or find parent note
            var wlParentNote = findObject(wunderlist.data.notes, 'task_id', wlTask.id);

            if (!wlParentNote) {
                var wlParentNoteId = noteIdFactory();
                wlParentNote = noteForThingsTodo(project, wlParentNoteId, wlTask.id);
                wunderlist.data.notes.push(wlParentNote);
            } else {
                wlParentNote.content += '\n\n';
            }

            wlParentNote.content += '::' + todo.name + '::';

            if (mustMergeNotes) {
                wlParentNote.content += '\n' + todo.notes;
            }
            if (mustMergeDates) {
                if (todo.dueDate) {
                    wlParentNote.content += '\n' + config.consolidateDueDate + ': ' + todo.dueDate;
                }
                if (todo.activationDate) {
                    wlParentNote.content += '\n' + config.consolidateActivationDate + ': ' + todo.activationDate;
                }
            }
        }
    }

    function appendThingsTodosAsSubtasks(project, wlTask) {
        var projectTodoId, currentProjectTodo,
            wlSubtaskPosition, wlSubtaskId, wlSubtask;

        /* this seems to be a project */
        var projectTodos = project.todos;
        if (projectTodos) {
            for (projectTodoId in projectTodos) {
                currentProjectTodo = projectTodos[projectTodoId];

                // create subtask
                wlSubtaskId = subtaskIdFactory();
                wlSubtask = subtaskForThingsTodo(currentProjectTodo, {
                    wlTaskId: wlTask.id,
                    wlSubtaskId: wlSubtaskId,
                    creator: config.user
                });
                wunderlist.data.subtasks.push(wlSubtask);

                // save position
                wlSubtaskPosition = findObject(wunderlist.data.subtask_positions, 'task_id', wlTask.id);
                if (!wlSubtaskPosition) {
                    wlSubtaskPosition = subtaskPositionForId(wlTask.id);
                    wunderlist.data.subtask_positions.push(wlSubtaskPosition);
                }

                wlSubtaskPosition.values.push(wlSubtaskId);
                mergeTodoIntoProject(currentProjectTodo, project, wlTask);
            }
        }
    }
}
