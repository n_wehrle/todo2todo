var config = {
	lists: ['Inbox','Today','Next','Scheduled','Someday'],

	/*
	    - todo: handle errors
	*/
	outfile: '~/Desktop/things-export.json'
};

// new code, ObjC Bridge, writes UTF8
var things = things2json(config);


// write to file, todo: define function for it
var evaluatedOutfileName = $.NSString.alloc.initWithUTF8String(config.outfile).stringByExpandingTildeInPath;
var bridgedString = $.NSString.alloc.initWithUTF8String(JSON.stringify(things));
bridgedString.writeToFileAtomicallyEncodingError(evaluatedOutfileName, true, $.NSUTF8StringEncoding, null);

/* ========================================================================== */

function things2json(config)  {
	'use strict';

	var result = {
		config: {},
		data: {},
		contacts: {}
	};


	var  app = Application('Things'),
	    isTrashed = todoFilter(app, 'Trash');

    // fill the configure block with life
    configureResult(result);

	// create the time based lists
	config.lists.forEach(function(listName) {
		handleList(listName, result.data, app.lists[listName].toDos);
	});

	// create the contact based lists
	var contacts = app.contacts;
	if (contacts) {
		for (var h=0; h<contacts.length; h++) {
			var currentContact = contacts[h];
			handleList(currentContact.name(), result.contacts, currentContact.toDos);
		}
	}

	return result;

	/* ====================================================================== */

	function handleList(listName, parentNode, outerTodos) {
		parentNode[listName] = {
			todos: {},
			projects: {}
		};

		var outerTodosLength = outerTodos.length;
		for (var i=0; i<outerTodosLength; i++) {
			var currentOuterTodo = outerTodos[i];
			if (!currentOuterTodo.project() && !currentOuterTodo.toDos.length) {
				/* simple todo, not in a project */
				if (currentOuterTodo.status() === 'open' && !isTrashed(currentOuterTodo)) {
					parentNode[listName].todos[currentOuterTodo.id()] = (objectForTodo(currentOuterTodo));
				}
			} else if (currentOuterTodo.project()) {
				/* simple todo within a project */
				appendProjectToNode(currentOuterTodo.project(), parentNode[listName].projects);
			} else if (currentOuterTodo.toDos.length) {
				/* i am a project */
				appendProjectToNode(currentOuterTodo, parentNode[listName].projects);
			}
		}
	}

	function configureResult(result) {
		result.config.areas = [];

		var areas = app.areas;
		var areasLength = app.areas.length;
		for (var i=0; i<areasLength; i++) {
			var currentArea = areas[i];
			result.config.areas.push(
				{
					id: currentArea.id(),
					name: currentArea.name()
				}
			);
		}
	}

	function appendProjectToNode(project, node) {
		var id = project.id();
		if (!node[id]) {
			node[id] = objectForTodo(project);
			node[id].todos = {};
			appendOpenTodosToNode(project.toDos, node[id].todos);
		}
	}


	function appendOpenTodosToNode(todos, node) {
		var length = todos.length;
		for (var j=0; j<length; j++){
			var currentTodo = todos[j];
			if (currentTodo.status() === 'open' && !isTrashed(currentTodo)) {
				node[currentTodo.id()] = objectForTodo(currentTodo);
			}
		}
	}

	function objectForTodo(todo) {
		var object = {
			id                : todo.id(),
			name              : todo.name(),
			status            : todo.status(),
			notes             : todo.notes(),
			creationDate      : todo.creationDate(),
			modificationDate  : todo.modificationDate(),
		};


		var tagNames = todo.tagNames();
		if (tagNames) {
			object.tagNames = tagNames.split(/,\s*/);
		}

		var area = todo.area();
		if (area) {
			object.area = {
				id: area.id(),
				name: area.name()
			};
		}

		var dueDate = todo.dueDate();
		if (dueDate) {
			object.dueDate = dueDate;
		}

		var activationDate = todo.activationDate();
		if (activationDate) {
			object.activationDate = activationDate;
		}

		return object;
	}

	function todoFilter(app, listName) {
		var map = {},
		todos = app.lists[listName].toDos,
		length = todos.length;

		for (var i=0; i<length; i++) {
			var id = todos[i].id();
			map[id] = true;
		}

		return function(todo) {
			return !!map[todo.id()];
		};
	}
}

/* ========================================================================== */
