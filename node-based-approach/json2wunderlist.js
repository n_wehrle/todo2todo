var fs = require('fs');

var config = {
	/* the user-id in wunderlist */
	user: 14374446,

	importFile: '/Users/nwe/Desktop/things-export.json',
	exportFile: '/Users/nwe/Desktop//wunderlist-import.json',

	/* this focuslist will be suppressed because all the task are in 'Next', to */
	todayFocusListName: 'Today',

	/* all items without an area will be saved here */
	defaultArea: {name: 'No Area', id: 'defaultAreaId'},

	/* all items from focuslist 'Inbox' will be saved in this wunderlist */
	inboxArea: {name: 'Inbox', id: 'inboxAreaId'},

	/* focuslists and projects will get special tags in wunderlist */
	tagNames: {
		Next: '#next',
		Scheduled: '#scheduled',
		Someday: '#someday',
		Project: '#project'
	},

	/* remind me at 8 o'clock */
	reminderOffset: 8,

	/*
		subtasks in things can have metadata.
		subtasks in wunderlist cannot.
		if true, metadata from things will be merged into the parent task
	*/
	consolidateTags: true,
	consolidateNotes: true,
	consolidateDates: true,

	/* Dates will become notes when merged, here are the labels */
	consolidateDueDate: 'due',
	consolidateActivationDate: 'activate'
};

var things = require(config.importFile);

var wunderlist = {
	user: config.user,
	exported: (new Date()).toISOString(),
	data: {
		lists: [],
		tasks: [],
		reminders: [],
		subtasks: [],
		notes: [],
		task_positions: [],
		subtask_positions: []
	}
};

// generate unique ids for each object type
var listIdFactory = idFactory();
var taskIdFactory = idFactory();
var noteIdFactory = idFactory();
var reminderIdFactory = idFactory();
var subtaskIdFactory = idFactory();


//do json transfromations
createWunderLists(things, wunderlist);

//write the file back
fs.writeFile(config.exportFile, JSON.stringify(wunderlist));

function createWunderLists(things, wunderlist) {
	for (var focusListName in things.data) {
		//exclude the focuslist for today, the task are in the next-list, too
		if (config.todayFocusListName != focusListName) {
			createEntriesForTodos(focusListName, things.data[focusListName].todos);
			createEntriesForTodos(focusListName, things.data[focusListName].projects);
		}
	}
}

function createEntriesForTodos(focusListName, todos) {
	'use strict';
	var todoId, currentTodo, currentArea;
	var wList, wlId;
	var wlTaskPosition, wlTask, wlTaskId;
	var wlNote, wlNoteId, wlReminderId, wlReminder;
	var putStarToTasks;

	for (todoId in todos) {
		currentTodo = todos[todoId];

		if (focusListName === 'Inbox') {
			currentArea = config.inboxArea;
		} else {
			currentArea = currentTodo.area || config.defaultArea;
		}

		wList = findObject(wunderlist.data.lists, 'things_area_id', currentArea.id);

		if (!wList) {
			wlId = listIdFactory();

			wList = listForThingsArea(currentArea, wlId);
			wunderlist.data.lists.push(wList);

			wlTaskPosition = taskPositionForId(wlId);
			wunderlist.data.task_positions.push(wlTaskPosition);
		} else {
			wlId = wList.id;
			wlTaskPosition = findObject(wunderlist.data.task_positions, 'id', wlId);
		}

		wlTaskId = taskIdFactory();

		//this todo is also in the today focuslist
		putStarToTasks = things.data[config.todayFocusListName]
	        && things.data[config.todayFocusListName].todos
			&& !!things.data[config.todayFocusListName].todos[currentTodo.id];

		wlTask = taskForThingsTodo(currentTodo, {
			wlTaskId: wlTaskId,
			wlId: wlId,
			creator: config.user,
			star: putStarToTasks,
			focusTag: config.tagNames[focusListName]
		});
		wunderlist.data.tasks.push(wlTask);
		wlTaskPosition.values.push(wlTaskId);

		if (currentTodo.notes) {
			wlNoteId = noteIdFactory();
			wlNote = noteForThingsTodo(currentTodo, wlNoteId, wlTaskId);
			wunderlist.data.notes.push(wlNote);
		}

		if (currentTodo.activationDate) {
			wlReminderId = reminderIdFactory();
			wlReminder = reminderForThingsTodo(currentTodo, wlReminderId, wlTaskId, config.reminderOffset);
			wunderlist.data.reminders.push(wlReminder);
		}

		appendThingsTodosAsSubtasks(currentTodo, wlTask);
	}
}

function findObject(array, key, value) {
	if (null === array) return false;
	var arrayLength = array.length;
	for (var i=0; i< arrayLength; i++) {
		var object = array[i];
		if (key) {
			if (object[key] === value) {
				return object;
			}
		} else {
			if (object === value) {
				return object;
			}
		}
	}
	return undefined;
}

function idFactory() {
	var id = 1;

	return function() {
		return id++;
	};
}

function listForThingsArea(area, wlId) {
	return {
		id             : wlId,
		created_at     : (new Date()).toISOString(),
		list_type      : 'list',
		'public'       : false,
		revision       : 1,
		title          : area.name,
		type           : 'list',
		things_area_id : area.id
	};
}

function taskPositionForId(wlId) {
	return {
		id       : wlId,
		list_id  : wlId,
		revision : 1,
		values   : [],
		type     : 'task_position'
	};
}

function subtaskPositionForId(wlTaskId) {
	return {
		id       : wlTaskId,
		task_id  : wlTaskId,
		revision : 1,
		values   : [],
		type     : 'subtask_position'
	};
}

function taskForThingsTodo(todo, params) {
	var wlTask = {
		id             : params.wlTaskId,
		created_at     : todo.creationDate,
		created_by_id  : params.creator,
		completed      : false,
		starred        : params.star,
		list_id        : params.wlId,
		revision       : 1,
		title          : todo.name,
		type           : 'task',
		things_todo_id : todo.id,
		things_name    : todo.name,
		things_tagNames: todo.tagNames
	};

	if (params.focusTag) {
		wlTask.title += ' ' + params.focusTag;
	}

	if (todo.todos) {
		wlTask.title += ' ' + config.tagNames.Project;
	}

	if (todo.dueDate) {
		var dueDate = new Date(todo.dueDate);
		wlTask.due_date = dueDate.getFullYear() + '-' + (dueDate.getMonth() + 1) + '-' + dueDate.getDate();
	}

	if (todo.tagNames) {
		todo.tagNames.forEach(function(tagName) {
			wlTask.title += ' #' + tagName.replace(/\s/g, '-');
		});
	}

	return wlTask;
}

function subtaskForThingsTodo(todo, params) {
	var wlSubtask = {
		id             : params.wlSubtaskId,
		created_at     : todo.creationDate,
		created_by_id  : params.creator,
		completed      : false,
		task_id        : params.wlTaskId,
		revision       : 1,
		title          : todo.name,
		type           : 'subtask',
		things_todo_id : todo.id,
		things_name    : todo.name,
		things_tagNames: todo.tagNames
	};

	return wlSubtask;
}

function noteForThingsTodo(todo, wlNoteId, wlTaskId) {
	return       {
		id          : wlNoteId,
		revision    : 1,
		content     : todo.notes,
		type        : 'note',
		task_id     : wlTaskId
	};
}

function reminderForThingsTodo(todo, wlReminderId, wlTaskId, reminderOffset) {
	var reminderDate = new Date(todo.activationDate);
	reminderDate.setHours(reminderOffset);

	return {
		id         : wlReminderId,
		date       : reminderDate.toISOString(),
		task_id    : wlTaskId,
		revision   : 1,
		created_at : todo.creationDate,
		updated_at : todo.modificationDate,
		type       : "reminder"
	};
}

function mergeTodoIntoProject(todo, project, wlTask) {
	// merge tags to parent
	if (config.consolidateTags && todo.tagNames) {
		todo.tagNames.forEach(function(tagName) {
			if (!project.tagNames || !findObject(project.tagNames, null, tagName)) {
				wlTask.title += ' #' + tagName.replace(/\s/g, '-');
			}
		});
	}

	// merge into parent note
	var mustMergeNotes = config.consolidateNotes && todo.notes;
	var mustMergeDates = config.consolidateDates && (todo.dueDate || todo.activationDate);
	var mustMergeAnything = mustMergeNotes || mustMergeDates;


	if (mustMergeAnything) {
		// create or find parent note
		var wlParentNote = findObject(wunderlist.data.notes, 'task_id', wlTask.id);

		if (!wlParentNote) {
			var wlParentNoteId = noteIdFactory();
			wlParentNote = noteForThingsTodo(project, wlParentNoteId, wlTask.id);
			wunderlist.data.notes.push(wlParentNote);
		} else {
			wlParentNote.content += '\n\n';
		}

		wlParentNote.content += '::' + todo.name + '::';

		if (mustMergeNotes) {
			wlParentNote.content += '\n' + todo.notes;
		}
		if (mustMergeDates) {
			if (todo.dueDate) {
				wlParentNote.content += '\n' + config.consolidateDueDate + ': ' + todo.dueDate;
			}
			if (todo.activationDate) {
				wlParentNote.content += '\n' + config.consolidateActivationDate + ': ' + todo.activationDate;
			}
		}
	}
}

function appendThingsTodosAsSubtasks(project, wlTask) {
	var projectTodoId, currentProjectTodo;
	var wlSubtaskPosition, wlSubtaskId, wlSubtask;

	/* this seems to be a project */
	var projectTodos = project.todos;
	if (projectTodos) {
		for (projectTodoId in projectTodos) {
			currentProjectTodo = projectTodos[projectTodoId];

			// create subtask
			wlSubtaskId = subtaskIdFactory();
			wlSubtask = subtaskForThingsTodo(currentProjectTodo, {
				wlTaskId: wlTask.id,
				wlSubtaskId: wlSubtaskId,
				creator: config.user
			});
			wunderlist.data.subtasks.push(wlSubtask);

			// save position
			wlSubtaskPosition = findObject(wunderlist.data.subtask_positions, 'task_id', wlTask.id);
			if (!wlSubtaskPosition) {
				wlSubtaskPosition = subtaskPositionForId(wlTask.id);
				wunderlist.data.subtask_positions.push(wlSubtaskPosition);
			}

			wlSubtaskPosition.values.push(wlSubtaskId);
			mergeTodoIntoProject(currentProjectTodo, project, wlTask);
		}
	}
}
